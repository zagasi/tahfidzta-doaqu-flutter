import 'package:flutter/material.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';

class DetailKegiatan extends StatefulWidget {
  final String img, title, date, content;
  DetailKegiatan(this.img, this.title, this.date, this.content);

  @override
  _DetailKegiatanState createState() => _DetailKegiatanState();
}

class _DetailKegiatanState extends State<DetailKegiatan> {
  void initState() {
    super.initState();
    print('Detail $widget.title was opened!');
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          Base.appName,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: TahDoPallete.green1,
        elevation: 0,
      ),
      body: Container(
        margin: (EdgeInsets.only(left: 16, right: 16)),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 16)),
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(widget.img),
                    fit: BoxFit.cover
                  ),
                    color: Colors.black26,
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12,
                          blurRadius: 4,
                          offset: Offset(0, 4)),
                    ],
                    ),
                height: 200,
                width: double.infinity,
              ),
              Padding(padding: EdgeInsets.only(top: 16)),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  widget.title,
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: FontSize.h6,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 4)),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  widget.date,
                  style: TextStyle(
                      color: Colors.black54, fontSize: FontSize.body2),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 16)),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  widget.content,
                  style: TextStyle(
                      color: Colors.black87, fontSize: FontSize.body1),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 32)),
            ],
          ),
        ),
      ),
    ));
  }
}
