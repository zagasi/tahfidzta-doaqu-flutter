import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:tahfidzta_doaqu/api/api_base.dart';
import 'package:tahfidzta_doaqu/api/api_service.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/debug.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'package:tahfidzta_doaqu/page/ayat/sambung_view.dart';
import 'package:tahfidzta_doaqu/page/ayat/tirukan_view.dart';
import 'package:tahfidzta_doaqu/page/kalender/kalender_view.dart';
import 'package:tahfidzta_doaqu/page/kegiatan/kegiatan_view.dart';
import 'package:tahfidzta_doaqu/page/lokasi/lokasi_view.dart';
import 'package:tahfidzta_doaqu/page/news/softlaunching_view.dart';
import 'package:tahfidzta_doaqu/page/santri/login_view.dart';
import 'package:tahfidzta_doaqu/page/sholat/sholat_view.dart';
import 'package:tahfidzta_doaqu/page/tentang/tentang_view.dart';
import 'package:tahfidzta_doaqu/page/sholat/sholat_respone.dart' as sholat;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _imgBanner1 = 'assets/images/bg1.jpg',
      _imgBanner2 = 'assets/images/bg3.jpg';

  double _iconContainerSize = 50,
      _iconSize = 25,
      _iconFontSize = FontSize.body2;
  var _iconFontWeight = FontWeight.w300;

  var _currentLocation = '',
      _currentCity = '',
      _latitudeData = '',
      _longitudeData = '';

  var _fajr = '', _dhuhr = '', _asr = '', _maghrib = '', _isha = '';
  var _currentTime = 0;
  var _currentSholat = '', _currentTimeSholat = '--:--';

  final ApiService _apiService = ApiService();

  void initState() {
    super.initState();
    _getCurrentLocation();
    print('[Page] Home');
  }

  Future _getCurrentLocation() async {
    try {
      Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
      geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
          .then((locationData) async {
        _latitudeData = locationData.latitude.toString();
        _longitudeData = locationData.longitude.toString();
        List<Placemark> listPlace = await geolocator.placemarkFromCoordinates(
            locationData.latitude, locationData.longitude);
        Placemark place = listPlace[0];
        setState(() {
          _currentLocation = place.locality;
          _currentCity = place.subAdministrativeArea;
          _getSholatTimeToday();
        });
        Debug().locationData(
            _latitudeData, _longitudeData, _currentLocation, _currentCity);
      }).catchError((error) {
        debugPrint('Geolocator Error: $error');
      });
    } catch (error) {
      print('Get current location error: $error');
    }
  }

  Future _getSholatTimeToday() async {
    var urlApi;
    await ApiUrl.sholatSchedule(
            ApiUrl.methodToday, _latitudeData, _longitudeData)
        .then((value) => urlApi = value);
    _apiService.get(
        url: urlApi,
        headers: {},
        callback: (status, message, respone) {
          try {
            setState(() {
              if (status) {
                sholat.ResponseSholat responseSholat =
                    sholat.ResponseSholat.fromJson(respone);
                List<sholat.Datetime> _listDateTime =
                    responseSholat.results.datetime;

                _fajr = _listDateTime[0].times.fajr;
                _dhuhr = _listDateTime[0].times.dhuhr;
                _asr = _listDateTime[0].times.asr;
                _maghrib = _listDateTime[0].times.maghrib;
                _isha = _listDateTime[0].times.isha;

                var _curTime = Func.timeToInt(Func.getTime(Format.time_3));
                var _iShubuh = Func.timeToInt(_fajr);
                var _iDhuhur = Func.timeToInt(_dhuhr);
                var _iAshar = Func.timeToInt(_asr);
                var _iMaghrib = Func.timeToInt(_maghrib);
                var _iIsya = Func.timeToInt(_isha);

                if (_curTime >= _iShubuh && _curTime < _iDhuhur) {
                  _currentSholat = Time.dhuhur;
                  _currentTimeSholat = _listDateTime[0].times.dhuhr;
                } else if (_curTime >= _iDhuhur && _curTime < _iAshar) {
                  _currentSholat = Time.ashar;
                  _currentTimeSholat = _listDateTime[0].times.asr;
                } else if (_curTime >= _iAshar && _curTime < _iMaghrib) {
                  _currentSholat = Time.maghrib;
                  _currentTimeSholat = _listDateTime[0].times.maghrib;
                } else if (_curTime >= _iMaghrib && _curTime < _iIsya) {
                  _currentSholat = Time.isya;
                  _currentTimeSholat = _listDateTime[0].times.isha;
                } else {
                  _currentSholat = Time.shubuh;
                  _currentTimeSholat = _listDateTime[0].times.fajr;
                }
                Debug().timeSholatData(
                    _currentTime, _fajr, _dhuhr, _asr, _maghrib, _isha);
              }
            });
          } catch (error) {
            print('Get sholat time error: $error');
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            Base.appName,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            IconButton(
              onPressed: () {
                _getCurrentLocation();
              },
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              icon: Icon(
                Icons.refresh_rounded,
                color: Colors.black,
                size: FontSize.h5,
              ),
            ),
          ],
        ),
        body: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            _waktuSholat(),
            Padding(
              padding: EdgeInsets.only(top: 16),
              child: _cardSoftLaunching(),
            ),
            Padding(
              padding: EdgeInsets.only(left: 16, top: 16, right: 16),
              child: Text(
                'Selamat Datang',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: FontSize.h5),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8),
              child: _menuIconGrid(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _waktuSholat() {
    var sholat = _currentSholat;
    var timeSholat = _currentTimeSholat;
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12, blurRadius: 4, offset: Offset(0, 4)),
            ],
            image: DecorationImage(
                image: AssetImage(_imgBanner1),
                fit: BoxFit.fill,
                colorFilter:
                    ColorFilter.mode(TahDoPallete.green1, BlendMode.multiply))),
        height: 120,
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Waktu Saat ini',
                  style: TextStyle(fontSize: FontSize.h6, color: Colors.white),
                ),
                StreamBuilder(
                  stream: Stream.periodic(Duration(seconds: 1)),
                  builder: (context, snapshot) {
                    return Text(
                      DateFormat.Hm().format(DateTime.now()),
                      style: TextStyle(
                          fontSize: FontSize.h3,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    );
                  },
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Menuju $sholat',
                  style: TextStyle(fontSize: FontSize.h6, color: Colors.white),
                ),
                Text(
                  '$timeSholat',
                  style: TextStyle(
                      fontSize: FontSize.h3,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _cardSoftLaunching() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      child: InkWell(
        onTap: () {
          print('Soft launching was pressed!');
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => SoftLaunchingNews()));
        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12, blurRadius: 4, offset: Offset(0, 4)),
              ],
              image: DecorationImage(
                image: AssetImage(_imgBanner2),
                fit: BoxFit.fill,
              )),
          height: 120,
          width: double.infinity,
          child: Center(
            child: Text(
              'SOFT LAUNCHING\nTAHFIZTA DOAQU',
              style: TextStyle(
                color: Colors.white,
                fontSize: FontSize.h5,
                fontWeight: FontWeight.w300,
                letterSpacing: 5,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _menuIconGrid() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(color: Colors.black12, blurRadius: 4, offset: Offset(0, 4))
        ],
        border: Border.all(color: Colors.black12, width: 0.2),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: 80,
                width: 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print('[Icon Menu] Tirukan Ayat');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TirukanAyatPage()));
                      },
                      child: Container(
                        height: _iconContainerSize,
                        width: _iconContainerSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: TahDoPallete.green1),
                        child: Icon(
                          Icons.menu_book_rounded,
                          color: Colors.white,
                          size: _iconSize,
                        ),
                      ),
                    ),
                    Text('Ayat',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: _iconFontSize,
                            fontWeight: _iconFontWeight)),
                  ],
                ),
              ),
              Container(
                height: 80,
                width: 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print('[Icon Menu] Sambung Ayat');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SambungAyatPage()));
                      },
                      child: Container(
                        height: _iconContainerSize,
                        width: _iconContainerSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: TahDoPallete.green2),
                        child: Icon(
                          Icons.playlist_play_rounded,
                          color: Colors.white,
                          size: _iconSize,
                        ),
                      ),
                    ),
                    Text('Sambung',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: _iconFontSize,
                            fontWeight: _iconFontWeight)),
                  ],
                ),
              ),
              Container(
                height: 80,
                width: 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print('[Menu Icon] Kegiatan Yayasan');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => KegiatanPage()));
                      },
                      child: Container(
                        height: _iconContainerSize,
                        width: _iconContainerSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.pink),
                        child: Icon(
                          Icons.receipt_long_rounded,
                          color: Colors.white,
                          size: _iconSize,
                        ),
                      ),
                    ),
                    Text('Kegiatan',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: _iconFontSize,
                            fontWeight: _iconFontWeight)),
                  ],
                ),
              ),
              Container(
                height: 80,
                width: 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print('[Menu Icon] Jadwal Sholat');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WaktuSholatPage(
                                      latitude: _latitudeData,
                                      longitude: _longitudeData,
                                      location: _currentLocation,
                                      fajr: _fajr,
                                      dhuhr: _dhuhr,
                                      asr: _asr,
                                      maghrib: _maghrib,
                                      isha: _isha,
                                    )));
                      },
                      child: Container(
                        height: _iconContainerSize,
                        width: _iconContainerSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.purple),
                        child: Icon(
                          Icons.access_time_rounded,
                          color: Colors.white,
                          size: _iconSize,
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(4)),
                    Text('Sholat',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: _iconFontSize,
                            fontWeight: _iconFontWeight)),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(8),
            child: Divider(
              thickness: 1,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: 80,
                width: 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print('[Menu Icon] Kalender Islam');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => KalenderIslam()));
                      },
                      child: Container(
                        height: _iconContainerSize,
                        width: _iconContainerSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.blue),
                        child: Icon(
                          Icons.calendar_today_rounded,
                          color: Colors.white,
                          size: _iconSize,
                        ),
                      ),
                    ),
                    Text('Kalender',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: _iconFontSize,
                            fontWeight: _iconFontWeight)),
                  ],
                ),
              ),
              Container(
                height: 80,
                width: 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print('[Menu Icon] Wali Santri');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                      },
                      child: Container(
                        height: _iconContainerSize,
                        width: _iconContainerSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.deepOrange),
                        child: Icon(
                          Icons.person_search,
                          color: Colors.white,
                          size: _iconSize,
                        ),
                      ),
                    ),
                    Text('Santri',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: _iconFontSize,
                            fontWeight: _iconFontWeight)),
                  ],
                ),
              ),
              Container(
                height: 80,
                width: 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print('[Icon Menu] Lokasi Yayasan');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LokasiPage()));
                      },
                      child: Container(
                        height: _iconContainerSize,
                        width: _iconContainerSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.blueGrey),
                        child: Icon(
                          Icons.pin_drop_rounded,
                          color: Colors.white,
                          size: _iconSize,
                        ),
                      ),
                    ),
                    Text('Lokasi',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: _iconFontSize,
                            fontWeight: _iconFontWeight)),
                  ],
                ),
              ),
              Container(
                height: 80,
                width: 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print('[Icon Menu] Tentang Yayasan');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TentangPage()));
                      },
                      child: Container(
                        height: _iconContainerSize,
                        width: _iconContainerSize,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.deepPurple),
                        child: Icon(
                          Icons.home_work_rounded,
                          color: Colors.white,
                          size: _iconSize,
                        ),
                      ),
                    ),
                    Text('Tentang',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: _iconFontSize,
                            fontWeight: _iconFontWeight)),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
