import 'package:flutter/material.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'package:tahfidzta_doaqu/page/santri/dashboard_view.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _nisTextController = TextEditingController(),
      _birthdateTextController = TextEditingController();
  String _nis = '', _birthdate = '';

  bool _showError = false, _nisValidate = false, _birthdateValidate = false;

  @override
  void initState() {
    print('[Page] | Login Page');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text(
                Base.appName,
                style: TextStyle(color: Colors.white),
              ),
              centerTitle: true,
              backgroundColor: TahDoPallete.green1,
              elevation: 0,
            ),
            body: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.only(left: 32, right: 32),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 32, bottom: 32),
                      child: Text(
                        'Masuk Untuk Melanjutkan',
                        style: TextStyle(
                            fontSize: FontSize.h6, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Text('Nomor Induk Santri'),
                    Padding(
                      padding: EdgeInsets.only(top: 8, bottom: 32),
                      child: TextField(
                        controller: _nisTextController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            errorText: _nisValidate
                                ? 'Nomor Induk Siswa Kosong'
                                : null),
                      ),
                    ),
                    Text('Tanggal Lahir Santri'),
                    Padding(
                      padding: EdgeInsets.only(top: 8, bottom: 8),
                      child: TextFormField(
                        controller: _birthdateTextController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            errorText: _birthdateValidate
                                ? 'Tanggal Lahir Siswa Kosong'
                                : null),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8, bottom: 8),
                      child: Visibility(
                        visible: _showError,
                        child: Text(
                          'Nomor Induk Santri atau Tanggal Lahir Santri salah, silahkan di cek kembali',
                          style: TextStyle(color: Colors.red),
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      child: RaisedButton(
                        onPressed: () {
                          setState(() {
                            String _pNis = 'B20.001';
                            String _pBday = '12032014';
                            _nis = _nisTextController.text;
                            _birthdate = _birthdateTextController.text;

                            if (_nis.isEmpty) {
                              _nisValidate = true;
                            }
                            if (_nis.isNotEmpty) {
                              _nisValidate = false;
                            }

                            if (_birthdate.isEmpty) {
                              _birthdateValidate = true;
                            }
                            if (_birthdate.isNotEmpty) {
                              _birthdateValidate = false;
                            }

                            if (_nis.toUpperCase() == _pNis.toUpperCase() &&
                                _birthdate.toUpperCase() ==
                                    _pBday.toUpperCase()) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          DashboardSantriPage()));
                              _nisTextController.clear();
                              _birthdateTextController.clear();
                            } else {
                              if (!_nisValidate && !_birthdateValidate) {
                                _showError = true;
                                _nisTextController.clear();
                                _birthdateTextController.clear();
                              }
                            }
                          });
                        },
                        color: TahDoPallete.green1,
                        child: Text(
                          'MASUK',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              letterSpacing: 2),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: InkWell(
                        highlightColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        onTap: () {
                          print('[Pressed] | Kendala Saat Masuk');
                        },
                        child: Text(
                          'Kendala saat masuk?',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.caption,
                              color: TahDoPallete.green1),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )));
  }
}
